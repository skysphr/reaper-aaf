# Reaper AAF

Script for importing [AAF files](https://en.wikipedia.org/wiki/Advanced_Authoring_Format "AAF files") into [REAPER](https://www.reaper.fm/ "REAPER").

## Installation and usage

1. Install Python 3.
1. Install [pyaaf2](https://pypi.org/project/pyaaf2/ "pyaaf2").
1. Configure REAPER to be able to load Python scripts: Preferences > Plug-ins > ReaScript > check "Enable Python for use with ReaScript". You might be required to locate the appropriate path for the Python .so or .dll.
1. Download the script and place it into the REAPER/Scripts resource directory (Options > Show resource path in explorer/finder).
1. Open the action list (Actions > Show action list) and click New action > Load ReaScript. If the Python environment is properly configured you should be able to select .py files. Select importaaf.py.
1. To import an AAF file into the current project, run the newly created action and select your file. A directory called "sources" containing the files embedded in the AAF will be created in the working directory of your project.

It is also possible to run the script directly from a terminal: `./importaaf.py file.aaf`. It will attempt to extract the audio data and print all the information gathered from the AAF. In case the script fails, this is a clearer way to see what went wrong.

## Features

- [x] Tested with AAF files exported from:
  - Premiere
  - Final Cut Pro AAF export plugin
  - ProTools
  - ProTools with MediaComposer compatibility
  - DaVinci Resolve
  - Vegas
  - DigitalPerformer
  - Logic Pro X
  - Cubase
- [x] Extract embedded audio, link to non-embedded source files
- [x] Panned mono tracks
- [x] Per-item volume
- [x] Volume/panning automation
- [x] Fades (linear and power)
- [x] Markers
- [x] Linked video clips

Tested on Linux and Windows.

## Documentation

See [docs](docs)

## Contributing

Please include script output when reporting issues, also including the AAF file if possible.

Discussion thread on Cockos forums [here](https://forum.cockos.com/showthread.php?t=262795).
